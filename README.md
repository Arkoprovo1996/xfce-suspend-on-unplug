# XFCE Suspend on Unplug

## What is it? (o_O)

An exremely simple script that runs every second in the background to make a laptop suspend when the AC power is disconnected, while the lid is closed (XFCE Power Manager doesn't, hence). This was the default behavior on Cinnamon.

## Installation 💃

Just clone the contents of the repository, or copy the `xfce-suspend-on-unplug` and `Suspend On Unplug.desktop`,  into the `~/.config/autostart/` directory.

Log out and log back in and it should be running in the background.

## Usage 😴

When the lid is closed, disconnect the AC Power (Unplug or switch off the mains), and the laptop should suspend, as is desired.

## Support 👨‍💻

For any feature requests, issues, etc., create an issue, or mail me at: `arkoprovo1996@disroot.org`

## Contributing 🍾

Feel free to create pull requests.

## Authors and acknowledgment 📚

Shoutout to the threads which helped me:

- [power management - Determine status of laptop lid - Unix &amp; Linux Stack Exchange](https://unix.stackexchange.com/questions/148197/determine-status-of-laptop-lid)

- [Toggling presentation mode via terminal. (xfce4-power-manager) / Desktop / Xfce Forums](https://forum.xfce.org/viewtopic.php?id=12746)

## License 🄯

GNU GPL 3
